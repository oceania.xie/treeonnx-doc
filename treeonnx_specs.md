# TreeONNX

TreeONNX is an intermediate representation(IR) of deep learning models inspired by and improved from ONNX(Open Neural Network eXchange).

ONNX is meant to serialization and compilation, and TreeONNX is meant to modularization and manipulation. So TreeONNX has the following key differences from ONNX:

- The model is hierarchically organized into a tree structure.
- The computational graph exists in different layers, flowing between leaf nodes and subtrees.
- The attributes/parameters(i.e. trained weights) are associated with each leaf node, ready for transferring and reusing.

## Protobuf3

```protobuf
syntax = "proto3";

import "onnx-copy.proto3";

message LeafNodeProto{
    repeated string inputs = 1;
    int num_output = 2;
    string name = 3;
    string op_type = 4;
    repeated onnxcopy.AttributeProto attrs = 5;
    repeated onnxcopy.TensorProto params = 6;
}

message RootNodeProto{
    string name = 1;
    repeated string inputs = 2;
    repeated string outputs = 3;
    repeated LeafNodeProto leaf_children = 4;
    repeated RootNodeProto root_children = 5;
}

option optimize_for = LITE_RUNTIME;

```

Leaf nodes(LeafNodeProto) and root nodes(RootNodeProto) are two key data structures of TreeONNX.

The nodes in the computational graph are organized in a tree structure, where LeafNodeProto are concrete nodes in the computational graph and RootNodeProto are higher level modules that consist of other RootNodeProto/LeafNodeProto nodes.

To see how the tree structure of TreeONNX is organized and how the computational graph is embedded in the names, check [here](#naming).

To see the supported LeafNodeProto operation types(*op_type*) and the specifications on *params* and *attrs*, check [here](#operators).

## <span id='naming'>Naming</span>

### Naming: Node Name

LeafNodeProto and RootNodeProto both have field *name* which indicates its location in the tree structure. A valid node name matches the following regular expression pattern:

```
^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*$
```

where "." indicates parent-child relation. For example, a root node named 'model' has a child named 'backbone', and the child 'backbone' has a child 'conv1'. When the intermediate node 'backbone' unfolds, 'conv1' is renamed as 'backbone.conv1' and becomes the child of 'model'. This example is visualized below.

Before unfolding:

```mermaid
graph TB
    model --> backbone
    model --> classifier
    backbone --> conv1
    backbone --> other
```

After unfolding:

```mermaid
graph TB
    model --> backbone.conv1
    model --> backbone.other
    model --> classifier
```

Similarly, provided the tree in the first figure, we can fold the nodes with the same prefix 'backbone' into a new subtree as the tree in the second figure.

### Naming: Inputs and Outputs Names

The *inputs* field of LeafNodeProto/RootNodeProto and the *outputs* field of RootNodeProto are string arrays, in which the names follow the pattern:

```
^[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*:([1-9][0-9]*|0)$
```

The names can be parsed as `<node name>:<index>`, where `<node name>` is either a sibling or `inputs`(the inputs of the parent). See the example below:

A root node 'model' has four blocks(leaf or root), denoted by round boxes. The inputs of the children and the outputs of the parent are denoted by square boxes.

```mermaid
stateDiagram-v2
    direction LR
    model --> block1
    note right of block1
        inputs: [inputs:0,]
    end note

    model --> block2
    note right of block2
        inputs: [inputs:0,]
    end note

    model --> block3
    note right of block3
        inputs: [block1:0, block2:0,]
    end note

    model --> block4
    note right of block4
        inputs: [block1:1, block3:0,]
    end note

    note left of model
        outputs: [block4:0,]
    end note
```

The computation flow can be parsed(round edge boxes are variables):

```mermaid
flowchart LR
    inputs0([inputs:0]) --> block1
    block1 --> block1_0([block1:0])
    block1 --> block1_1([block1:1])
    inputs0([inputs:0]) --> block2
    block2 --> block2_0([block2:0])
    block1_0 --> block3
    block2_0 --> block3
    block3 --> block3_0([block3:0])
    block3_0 --> block4
    block1_1 --> block4
    block4 --> block4_0([block4:0])
    block4_0 --> outputs
```

As you can find from the example, the *outputs* of the children and the *inputs* of the parent are not relevant to the internal computation flow. The *inputs* of a RootNodeProto are meaningful for its parent/siblings to determine the computation flow; the *outputs* of a RootNodeProto are meaningful for its children to determine which variables to output; the name of the outputs of a LeafNodeProto is useless, so LeafNodeProto has an integer field *num_outputs* instead.

## <span id='operators'>Leaf Node Operation/Attributes/Parameters</span>

The behavior of a leaf node is defined by  *LeafNodeProto.op_type*, *LeafNodeProto.attrs*, *LeafNodeProto.params*.

- *op_type*: The computation executed by this leaf node. Supported operations are listed in the next section.

- *attrs*: Constant attributes of the node, borrowed from ONNX. Here "Constant" means the attributes normally do not change in model training. Attributes can either be basic types(int, string, float, etc.) or tensor.

- *params*: Trainable tensors of the node, borrowed from ONNX. Parameters are trainable(often differentiable) tensors in the models.

## Supported Operators

This section lists all supported *op_type* of TreeONNX LeafNodeProto and specification on node input/output/parameters(*params*)/attributes(*attrs*).

Some specifications:
- Data types such as int/float/string/tensor[dtype]/List etc. are defined in ONNX AttributeProto and TensorProto.
- Tensor and list indexing supports Python-style indexing, which means for a tensor dimension or a list of length L, indices from -L to L-1 are valid.

- ***Neural Network***
  - [Linear](#linear)
- ***Convolution***
  - [Conv1d](#operator-conv1d)
  - [Conv2d](#operator-conv2d)
  - [Conv3d](#operator-conv3d)
- ***Activation***
  - [ReLU](#operator-relu)
  - [ReLU6](#operator-relu6)
  - [Sigmoid](#operator-sigmoid)
  - [Tanh](#operator-tanh)
  - [HardSigmoid](#operator-hardsigmoid)
  - [HardTanh](#operator-hardtanh)
  - [LeakyReLU](#operator-leakyrelu)
- ***Pooling***
  - [AdaptiveAvgPool2d](#operator-adaptiveavgpool2d)
  - [AdaptiveMaxPool2d](#operator-adaptivemaxpool2d)
  - [AvgPool2d](#operator-avgpool2d)
  - [GlobalAvgPool2d](#operator-globalavgpool2d)
  - [GlobalMaxPool2d](#operator-globalmaxpool2d)
  - [MaxPool2d](#operator-maxpool2d)
- ***Regularization***
  - [BatchNorm1d](#operator-batchnorm1d)
  - [BatchNorm2d](#operator-batchnorm2d)
  - [BatchNorm3d](#operator-batchnorm3d)
  - [Dropout](#operator-dropout)
- ***Tensor Computation***
  - [Add](#operator-add)
  - [Div](#operator-div)
  - [Max](#operator-max)
  - [Mean](#operator-mean)
  - [Min](#operator-min)
  - [Mul](#operator-mul)
  - [Sub](#operator-sub)
  - [Sum](#operator-sum)
- ***Tensor Transformation***
  - [Concat](#operator-concat)
  - [Flatten](#operator-flatten)
  - [Pad](#operator-pad)
  - [Shape](#operator-shape)
  - [Size](#operator-size)
  - [Split](#operator-split)
  - [Transpose](#operator-transpose)
- ***Misc***
  - [Constant](#operator-constant)
  - [ListConstruct](#operator-listconstruct)
  - [ListUnpack](#operator-listunpack)
  - [Parameter](#operator-parameter)

### <span id="operator-linear">**Linear**</span>

Performs a fully-connected layer operation.

#### Inputs

1. *X*: [T] Input tensor of shape $(N, C_{in})$.

#### Outputs

1. *Y*: [T] Out tensor of shape $(N, C_{out})$.

#### Attributes

None

#### Parameters

1. *weight*: [T] the learnable weights of shape $(C_{out}, C_{in})$.
2. *bias*: [T, optional] the optional learnable bias of shape $(C_{out})$.

#### Type Constraints

T: tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-conv1d">**Conv1d**</span>

Applies a 1D convolution over an input signal composed of several input planes.

#### Inputs

1. *X*: [T] Input tensor of shape $(N, C_{in}, L_{in})$.

#### Outputs

1. *Y*: [T] Out tensor of shape $(N, C_{out}, L_{out})$.

#### Attributes

1. *stride*: [int=1] Stride of the convolution. Greater than or equal to 1.
2. *padding*: [List[int=0]] Padding on the start and the end. Length is 2. Values greater than or equal to 0.
3. *dilation*: [int=1] The spacing between kernel points. Greater than or equal to 1.
4. *group*: [int=1] The connections between inputs and outputs. Greater than or equal to 1. $C_{in}$ and $C_{out}$ are divisible by *groups*.
5. *padding_mode*: [string='zeros'] Controls the values that fill the padding space, one of 'zeros', 'reflect', 'replicate' and 'circular'.

#### Parameters

1. *weight*: [T] the learnable weights of shape $(C_{out}, \frac{C_{in}}{groups}, K)$.
2. *bias*: [T, optional] the optional learnable bias of shape $(C_{out})$.

#### Type Constraints

T: tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-conv2d">**Conv2d**</span>

Applies a 2D convolution over an input signal composed of several input planes.

#### Inputs

1. *X*: [T] Input tensor of shape $(N, C_{in}, H_{in}, W_{in})$.

#### Outputs

1. *Y*: [T] Out tensor of shape $(N, C_{out}, H_{out}, W_{out})$.

#### Attributes

1. *stride*: [List[int=1]] Stride of the convolution at the two axes. Greater than or equal to 1. Length is 2.
2. *padding*: [List[int=0]] Padding on the start and the end at the two axes. Length is 4, each stands for H_start, W_start, H_end, W_end. Values greater than or equal to 0.
3. *dilation*: [List[int=1]] The spacing between kernel points at the two axes. Length is 2. Greater than or equal to 1.
4. *group*: [int=1] The connections between inputs and outputs. Greater than or equal to 1. $C_{in}$ and $C_{out}$ are divisible by *groups*.
5. *padding_mode*: [string='zeros'] Controls the values that fill the padding space, one of 'zeros', 'reflect', 'replicate' and 'circular'.

#### Parameters

1. *weight*: [T] the learnable weights of shape $(C_{out}, \frac{C_{in}}{groups}, K_{H}, K_{W})$.
2. *bias*: [T, optional] the optional learnable bias of shape $(C_{out})$.

#### Type Constraints

T: tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-conv3d">**Conv3d**</span>

Applies a 3D convolution over an input signal composed of several input planes.

#### Inputs

1. *X*: [T] Input tensor of shape $(N, C_{in}, D_{in}, H_{in}, W_{in})$.

#### Outputs

1. *Y*: [T] Out tensor of shape $(N, C_{out}, D_{out}, H_{out}, W_{out})$.

#### Attributes

1. *stride*: [List[int=1]] Stride of the convolution at the two axes. Greater than or equal to 1. Length is 3.
2. *padding*: [List[int=0]] Padding on the start and the end at the two axes. Length is 6, each stands for D_start, H_start, W_start, D_end, H_end, W_end. Values greater than or equal to 0.
3. *dilation*: [List[int=1]] The spacing between kernel points at the two axes. Length is 3. Greater than or equal to 1.
4. *group*: [int=1] The connections between inputs and outputs. Greater than or equal to 1. $C_{in}$ and $C_{out}$ are divisible by *groups*.
5. *padding_mode*: [string='zeros'] Controls the values that fill the padding space, one of 'zeros', 'reflect', 'replicate' and 'circular'.

#### Parameters

1. *weight*: [T] the learnable weights of shape $(C_{out}, \frac{C_{in}}{groups}, K_{D}, K_{H}, K_{W})$.
2. *bias*: [T, optional] the optional learnable bias of shape $(C_{out})$.

#### Type Constraints

T: tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-relu">**ReLU**</span>

Performs rectified linear unit operation element-wise.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] Result of the same shape as *tensor*.

#### Attributes

1. *inplace*:[int=0] Whether *result* will displace the storage of *tensor*.

#### Parameters

None

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-relu6">**ReLU6**</span>

Performs ReLU6 activation function. Equivalent to HardTanh(min_val=0, max_val=6).

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] Result of the same shape as *tensor*.

#### Attributes

1. *inplace*:[int=0] Whether *result* will displace the storage of *tensor*.

#### Parameters

None

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-sigmoid">**Sigmoid**</span>

Performs sigmoid function element-wise.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] Result of the same shape as *tensor*.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-tanh">**Tanh**</span>

Performs hyperbolic tangent function element-wise.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] Result of the same shape as *tensor*.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-hardsigmoid">**HardSigmoid**</span>

Performs hard sigmoid function element-wise.
$$
  y = max(0, min(1,  \frac{x}{6} + \frac{1}{2}))
$$

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] Result of the same shape as *tensor*.

#### Attributes

1. *inplace*:[int=0] Whether *result* will displace the storage of *tensor*.

#### Parameters

None

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-hardtanh">**HardTanh**</span>

Performs hard hyperbolic tangent function element-wise.
$$
  y = max(\beta, min(\alpha, x))
$$

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] Result of the same shape as *tensor*.

#### Attributes

1. *min_val*: [float=-1.0] Value of $\alpha$.
2. *max_val*: [float=1.0] Value of $\beta$.
3. *inplace*:[int=0] Whether *result* will displace the storage of *tensor*.

#### Parameters

None

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-leakyrelu">**LeakyReLU**</span>

Performs hard sigmoid function element-wise.
$$
  y = max(\alpha x, x)
$$

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] Result of the same shape as *tensor*.

#### Attributes

1. *alpha*: [float=0.01] Value of $\alpha\in [0, 1)$.

#### Parameters

None

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-adaptiveavgpool2d">**AdaptiveAvgPool2d**</span>

Applies a 2D adaptive average pooling over an input tensor composed of several input planes.

#### Inputs

1. *X*:[T] Input tensor of shape $(N, C, H, W)$.

#### Outputs

1. *Y*:[T] Output tensor of shape $(N, C, H', W')$. H' and W' equal the node attribute *output_size[0]* and *output_size[1]*.

#### Attributes

1. *output_size*: [List[int]] the output height/width of *Y*. Length is 2. All numbers greater than 0, respectively stands for H' and W'.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-adaptivemaxpool2d">**AdaptiveMaxPool2d**</span>

Applies a 2D adaptive max pooling over an input tensor composed of several input planes.

#### Inputs

1. *X*:[T] Input tensor of shape $(N, C, H, W)$.

#### Outputs

1. *Y*:[T] Output tensor of shape $(N, C, H', W')$. H' and W' equal the node attribute *output_size[0]* and *output_size[1]*.
2. *indices*:[tensor(int64)] Indices of max values.

#### Attributes

1. *output_size*: [List[int]] the output height/width of *Y*. Length is 2. All numbers greater than 0, respectively stands for H' and W'.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-avgpool2d">**AvgPool2d**</span>

Applies a 2D average pooling over an input tensor.

#### Inputs

1. *X*:[T] Input tensor of shape $(N, C, H, W)$.

#### Outputs

1. *Y*:[T] Output tensor of shape $(N, C, H', W')$.

#### Attributes

1. *kernel_shape*:[List[int]] The size of the kernel along each axis. All values are greater than 0. Length is 2.
2. *padding*:[List[int=0]] The size of padding to the start and end of each axis. All values are greater than or equal to 0. Length is 4, respectively stands for H_start, W_start, H_end, W_end.
3. *stride*:[List[int=1]] The stride along each axis. All values are greater than 0. Length is 2.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-globalavgpool2d">**GlobalAvgPool2d**</span>

Applies a 2D global average pooling over an input tensor.

#### Inputs

1. *X*:[T] Input tensor of shape $(N, C, H, W)$.

#### Outputs

1. *Y*:[T] Output tensor of shape $(N, C)$.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-globalmaxpool2d">**GlobalMaxPool2d**</span>

Applies a 2D global max pooling over an input tensor.

#### Inputs

1. *X*:[T] Input tensor of shape $(N, C, H, W)$.

#### Outputs

1. *Y*:[T] Output tensor of shape $(N, C)$.
2. *indices*:[tensor(int64)] Indices of max values.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-maxpool2d">**MaxPool2d**</span>

Applies a 2D max pooling over an input tensor.

#### Inputs

1. *X*:[T] Input tensor of shape $(N, C, H, W)$.

#### Outputs

1. *Y*:[T] Output tensor of shape $(N, C, H', W')$.
2. *indices*:[tensor(int64)] Indices of max values.

#### Attributes

1. *kernel_shape*:[List[int]] The size of the kernel along each axis. All values are greater than 0. Length is 2.
2. *padding*:[List[int=0]] The size of padding to the start and end of each axis. All values are greater than or equal to 0. Length is 4, respectively stands for H_start, W_start, H_end, W_end.
3. *stride*:[List[int=1]] The stride along each axis. All values are greater than 0. Length is 2.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-batchnorm1d">**BatchNorm1d**</span>

Carries out batch normalization for 1D feature tensors as described in the paper
https://arxiv.org/abs/1502.03167.

#### Inputs

1. *X*: [T] Input tensor of shape $(N, C)$ or $(N, C, L)$.

#### Outputs

1. *Y*: [T] Out tensor of shape $(N, C)$ or $(N, C, L)$.

#### Attributes

1. *eps*: [float=1e-05] The epsilon value to use to avoid division by zero.
2. *momentum*: [float=0.9] Factor used in computing running mean and running variance.

#### Parameters

1. *running_mean*: [T] The estimated mean tensor of shape $(C)$.
2. *running_var*: [T] The estimated variance tensor of shape $(C)$.
3. *scale*: [T, optional] Scale tensor of shape $(C)$. Required only when *bias* is provided.
4. *bias*: [T, optional] Bias tensor of shape $(C)$. Required only when *scale* is provided.

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double)

### <span id="operator-batchnorm2d">**BatchNorm2d**</span>

Carries out batch normalization for 2D feature tensors as described in the paper
https://arxiv.org/abs/1502.03167.

#### Inputs

1. *X*: [T] Input tensor of shape $(N, C, H, W)$.

#### Outputs

1. *Y*: [T] Out tensor of shape $(N, C, H, W)$.

#### Attributes

1. *eps*: [float=1e-05] The epsilon value to use to avoid division by zero.
2. *momentum*: [float=0.9] Factor used in computing running mean and running variance.

#### Parameters

1. *running_mean*: [T] The estimated mean tensor of shape $(C)$.
2. *running_var*: [T] The estimated variance tensor of shape $(C)$.
3. *scale*: [T, optional] Scale tensor of shape $(C)$. Required only when *bias* is provided.
4. *bias*: [T, optional] Bias tensor of shape $(C)$. Required only when *scale* is provided.

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double)

### <span id="operator-batchnorm3d">**BatchNorm3d**</span>

Carries out batch normalization for 3D feature tensors as described in the paper
https://arxiv.org/abs/1502.03167.

#### Inputs

1. *X*: [T] Input tensor of shape $(N, C, D, H, W)$.

#### Outputs

1. *Y*: [T] Out tensor of shape $(N, C, D, H, W)$.

#### Attributes

1. *eps*: [float=1e-05] The epsilon value to use to avoid division by zero.
2. *momentum*: [float=0.9] Factor used in computing running mean and running variance.

#### Parameters

1. *running_mean*: [T] The estimated mean tensor of shape $(C)$.
2. *running_var*: [T] The estimated variance tensor of shape $(C)$.
3. *scale*: [T, optional] Scale tensor of shape $(C)$. Required only when *bias* is provided.
4. *bias*: [T, optional] Bias tensor of shape $(C)$. Required only when *scale* is provided.

#### Type Constraints

T: tensor(float16), tensor(float), tensor(double)

### <span id="operator-dropout">**Dropout**</span>

Randomly mask(set to 0) a number of elements of the input tensor by *ratio*. The output is scaled by $\frac{1}{1-ratio}$.

#### Inputs

1. *X*: [T] Input tensor.

#### Outputs

1. *Y*: [T] Output tensor of the same shape as *X*. Some elements are set to 0 and others are scaled.

#### Attributes

1. *ratio*: [float] The probability that each element will be masked. $ratio\in [0, 1)$.

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-add">**Add**</span>

Performs element-wise addition (with Numpy-style broadcasting support) with scaling:

$$
  C = A + \alpha B
$$

#### Inputs

1. *A*:[T] First operand.
2. *B*:[T] Second operand.

#### Outputs

1. *C*:[T] Result.

#### Attributes

1. *alpha*[float=1.0]: scaling factor $\alpha$.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-mul">**Mul**</span>

Performs element-wise multiplication (with Numpy-style broadcasting support).

#### Inputs

1. *A*:[T] First operand.
2. *B*:[T] Second operand.

#### Outputs

1. *C*:[T] Result.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-div">**Div**</span>

Performs element-wise division (with Numpy-style broadcasting support).

#### Inputs

1. *A*:[T] First operand.
2. *B*:[T] Second operand.

#### Outputs

1. *C*:[T] Result.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-sub">**Sub**</span>

Performs element-wise subtraction (with Numpy-style broadcasting support) with scaling:
$$
  C = A - \alpha B
$$


#### Inputs

1. *A*:[T] First operand.
2. *B*:[T] Second operand.

#### Outputs

1. *C*:[T] Result.

#### Attributes

1. *alpha*[float=1.0]: scaling factor $\alpha$.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-max">**Max**</span>

Computes the max of the tensor elements along the provided axes.

#### Inputs

1. *X*:[T] Input tensor.

#### Outputs

1. *MaxValues*:[T] The max values along *axes*. If *keepdims* is true, *axes* have size 1, otherwise the dimension is removed.
2. *Indices*:[tensor(int64)] The max element indices along *axes*. If *keepdims* is true, *axes* have size 1, otherwise the dimension is removed.

#### Attributes

1. *axes*:[List[int]] The axes along which the max values are computed, each axis must be valid for *X* and not duplicate.
2. *keepdims*:[int=0] See the outputs.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-min">**Min**</span>

Computes the min of the tensor elements along the provided axes.

#### Inputs

1. *X*:[T] Input tensor.

#### Outputs

1. *MinValues*:[T] The min values along *axes*. If *keepdims* is true, *axes* have size 1, otherwise the dimension is removed.
2. *Indices*:[tensor(int64)] The min element indices along *axes*. If *keepdims* is true, *axes* have size 1, otherwise the dimension is removed.

#### Attributes

1. *axes*:[List[int]] The axes along which the min values are computed, each axis must be valid for *X* and not duplicate.
2. *keepdims*:[int=0] See the outputs.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-sum">**Sum**</span>

Computes the sum of the tensor elements along the provided axes.

#### Inputs

1. *X*:[T] Input tensor.

#### Outputs

1. *Y*:[T] The sums along *axes*. If *keepdims* is true, *axes* have size 1, otherwise the dimension is removed.

#### Attributes

1. *axes*:[List[int]] The axes along which the sums are computed, each axis must be valid for *X* and not duplicate.
2. *keepdims*:[int=0] See the outputs.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-mean">**Mean**</span>

Computes the mean of the tensor elements along the provided axes.

#### Inputs

1. *X*:[T] Input tensor.

#### Outputs

1. *Y*:[T] The means along *axes*. If *keepdims* is true, *axes* have size 1, otherwise the dimension is removed.

#### Attributes

1. *axes*:[List[int]] The axes along which the sums are computed, each axis must be valid for *X* and not duplicate.
2. *keepdims*:[int=0] See the outputs.

#### Parameters

None

#### Type Constraints

T: tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double)

### <span id="operator-concat">**Concat**</span>

Concatenate multiple tensors into one tensor along a provided axis.

#### Inputs

1. *tensor_list*:[List[T]] The dynamic array of tensors. Tensors have same data type and same shape except the axis along which to perform concatenation.

#### Outputs

1. *result*:[T] The concatenated tensor.

#### Attributes

1. *axis*:[int] The axis to perform concatenation.

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-flatten">**Flatten**</span>

Merge a selected range of dims of the tensor into one dim.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *result*:[T] The flattened tensor.

#### Attributes

1. *start_dim*:[int=0] the first dim to flatten
2. *end_dim*:[int=-1] the last dim to flatten.

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

#### <span id="operator-pad">**Pad**</span>

Augment the tensor at the start and end of provided axes by a specific padding mode.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *padded_tensor*:[T] Padded input tensor.

#### Attributes

1. *mode*:[string='constant'] Supported modes are 'constant', 'reflect' and 'replicate', 'circular'.
2. *pads*:[List[int]] The number of padding elements to the start or end of each axis. *pads* should be of length $2*tensor.dim$, respectively stands for D0_start, D1_start, ..., DN_start, D0_end, D1_end, ..., DN_end. All numbers are greater than or equal to 0.
3. *constant_value*:[T1, optional] A scalar value to be used if the mode is 'constant'.

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

T1: the underlying data type of T(e.g. int32 of tensor(int32))

### <span id="operator-shape">**Shape**</span>

Return the size of each dimension of the input tensor.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *shape*:[List[int]|int] The dimensions of the tensor or a specific length of a dimension.

#### Attributes

1. *dim*:[int, optional] When *dim* is not provided, returns the length of all dims; otherwise, returns the length of a specific dim.

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-size">**Size**</span>

Return the total number of elements of the input tensor.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *size*:[int] The total number of elements of *tensor*.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-split">**Split**</span>

Split a tensor into a list of tensors, along the attribute *axis*. Lengths of the parts can be specified using attribute *split*. Otherwise, the tensor is split to equal sized parts.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *tensor_list*:[List[T]] Split tensors.

#### Attributes

1. *axis*:[int] The axis to split on, must be valid for *tensor*.
2. *split*:[List[int], optional] Optional length of each output. Values should be >= 0. Sum of the values must be equal to the dim size that *axis* specified. If not specified, *split*=[1, 1, 1..., 1].

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-transpose">**Transpose**</span>

Permute two dimensions of a tensor.

#### Inputs

1. *tensor*:[T] Input tensor.

#### Outputs

1. *transposed_tensor*:[T] Transposed tensor.

#### Attributes

1. *perm*:[List[int]] If the $i$-th element of *perm* is $j$, then the $i$-th dimension of *transposed_tensor* is displaced by the $j$-th dimension of *tensor*. *perm* must contain all numbers from 0 to tensor.dim-1 without duplication.

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-constant">**Constant**</span>

Return a constant tensor defined as an attribute.

#### Inputs

None

#### Outputs

1. *Y*:[T] Output tensor of any shape.

#### Attributes

1. *constant*: [T] Output tensor.

#### Parameters

None

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-parameter">**Parameter**</span>

Return a differentiable tensor defined as parameter.

#### Inputs

None

#### Outputs

1. *Y*:[T] Output tensor of any shape.

#### Attributes

None

#### Parameters

1. *param*: [T] Output tensor.

#### Type Constraints

T: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-listconstruct">**ListConstruct**</span>

Construct a dynamic array of tensors from arbitrary number of tensors.

#### Inputs

1. *tensor1*:[T1] Tensor of any data type or shape.

...

N. *tensorN*:[TN] Tensor of any data type or shape.

#### Outputs

1. *tensor_list*:[List[T1|...|TN]] The dynamic array of the input tensors.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T1~TN: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)

### <span id="operator-listunpack">**ListUnpack**</span>

Unpack an arbitrary-length list of tensors.

#### Inputs

1. *tensor_list*:[List[T1|...|TN]] The dynamic array of the input tensors.

#### Outputs

1. *tensor1*:[T1] The first tensor in the tensor_list.

...

N. *tensorN*:[TN] The last tensor in the tensor_list.

#### Attributes

None

#### Parameters

None

#### Type Constraints

T1~TN: tensor(bool), tensor(uint8), tensor(uint16), tensor(uint32), tensor(uint64), tensor(int8), tensor(int16), tensor(int32), tensor(int64), tensor(float16), tensor(float), tensor(double), tensor(complex64), tensor(complex128)
